//
//  ViewController.swift
//  VistasiOS
//
//  Created by mastermoviles on 20/11/17.
//  Copyright © 2017 EPS. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    
    @IBOutlet weak var panel_notificaciones: UITextView!
    
    @IBOutlet weak var slider_hipervelocidad: UISlider!

    @IBOutlet weak var label_valor_hipervelocidad: UILabel!
    
    @IBOutlet weak var button_emergencia: UIButton!
    
    @IBOutlet weak var label_numero_incidencias: UILabel!
    
    @IBAction func cambiar_slider_hipervelocidad(_ sender: UISlider)
    {
        let valor_slider_hipervelocidad = String(format: "%.2f", sender.value)
        label_valor_hipervelocidad.text = valor_slider_hipervelocidad
    }
    
    @IBAction func anadirNotificacion(_ sender: UITextField)
    {
        sender.resignFirstResponder()
        
        let notificacion = sender.text;
        let notificaciones_actuales = self.panel_notificaciones.text
        
        self.panel_notificaciones.text = notificaciones_actuales! + notificacion! + "\n"
        
        sender.text = ""
    }
    
    @IBAction func abrirActionSheet(_ sender: UIButton)
    {
        let actionSheet = UIAlertController(title: "Opciones",
                                            message: "Seleccione la opción",
                                            preferredStyle: .actionSheet)
        let nave_salvavidas = UIAlertAction(title: "nave salvavidas", style: .default){
            action in
            print("nave salvavidas")
        }
        let hiperespacio = UIAlertAction(title: "hiperespacio", style: .default) {
            action in
            print("hiperespacio")
        }
        
        let autodestruccion = UIAlertAction(title: "autodestrucción", style: .destructive) {
            action in
            print("autodestrucción")
        }
    
        actionSheet.addAction(nave_salvavidas)
        actionSheet.addAction(hiperespacio)
        actionSheet.addAction(autodestruccion)
        
        present(actionSheet, animated: true)
    }
    
    
    @IBAction func switch_pulsado(_ sender: UISwitch)
    {
        if sender.isOn
        {
            self.button_emergencia.isEnabled = true
        }
        else
        {
            self.button_emergencia.isEnabled = false
        }
    }
    
    @IBAction func actualizarNumeroIncidencias(_ sender: UIStepper)
    {
        label_numero_incidencias.text = "\(Int(sender.value))"
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.slider_hipervelocidad!.maximumValue = 100;
        self.slider_hipervelocidad!.minimumValue = 0;

    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

